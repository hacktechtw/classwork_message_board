<?php
/**
 * Created by PhpStorm.
 * User: Sues Wu
 * Date: 2018/5/14
 * Time: 下午 01:08
 */
?>
<html>
    <header>
        <title>留言板</title>
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        {{-- Jquery --}}
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        {{-- Bootstrap --}}
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js" integrity="sha384-u/bQvRA/1bobcXlcEYpsEdFVK/vJs3+T+nXLsBYJthmdBuavHvAW6UsmqO2Gd/F9" crossorigin="anonymous"></script>

        {{-- SweetAlert --}}
        <link href="https://unpkg.com/sweetalert2@7.20.1/dist/sweetalert2.min.css" rel="stylesheet">
        <script src="https://unpkg.com/sweetalert2@7.20.1/dist/sweetalert2.all.min.js"></script>


        <style>
            .center-block{
                display: block;
                margin-right: auto;
                margin-left: auto;
            }
            .right-block{
                display: block;
                margin-right: 0;
                text-align:right;
                margin-left: auto;
            }
            .left-block{
                display: block;
                margin-right: auto;
                margin-left: 0;
            }
            .wall{
                min-height: 20px;
                padding: 19px;
                margin-bottom: 20px;
                background-color: #f5f5f5;
                border: 1px solid #e3e3e3;
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
            }
            .other-wall{
                min-height: 20px;
                padding: 19px;
                margin-bottom: 20px;
                background-color: #f5f5f5;
                border: 1px solid #e3e3e3;
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
            }
            .myself-wall{
                min-height: 20px;
                padding: 19px;
                margin-bottom: 20px;
                background-color: mediumseagreen;
                border: 1px solid #e3e3e3;
                color: white;
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
            }
            .white-wall{
                min-height: 20px;
                padding: 19px;
                margin-bottom: 20px;
                background-color: #000000;
                border: 1px solid #e3e3e3;
                color: white;
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
            }
        </style>
    </header>
    <body>
        <div class="container">
            <br>
            <div class="row">
                <div class="col-md-12">
                    @if(!empty($message_list))
                        @foreach($message_list as $item)
                            @if($myself_ip == $item->from_ip)
                                <div class="col-md-6 myself-wall right-block">
                                    <div>{{ $item->name or '' }}：</div>
                                    <div>{{ $item->msg_detail or '' }}</div>
                                    <div style="text-align: left;"><button type="button" value="{{ $item->id }}" class="btn btn-danger delete_button">刪除</button></div>
                                </div>
                            @else
                                <div class="col-md-6 other-wall left-block">
                                    <div>{{ $item->name or '' }}：</div>
                                    <div>{{ $item->msg_detail or '' }}</div>
                                    {{--<div style="text-align: right;"><button type="button" value="{{ $item->id }}" class="btn btn-danger delete_button">刪除</button></div>--}}
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>
                <div class="col-md-12">
                    <div class="col-md-8 white-wall center-block">
                        <form action="/message" method="post" class="form-horizontal">
                            <h3>留言</h3>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group form-group-lg">
                                <label class="col-sm-2 control-label" for="name">暱稱</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" id="name" name="name" placeholder="暱稱">
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label" for="password">保護密碼</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" id="password" name="password" placeholder="保護密碼">
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label" for="msg_detail">留言內容</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" type="text" id="msg_detail" name="msg_detail" placeholder="留言內容"></textarea>
                                </div>
                            </div>
                            <div class="form-group form-group-sm right-block">
                                <button type="submit" class="btn btn-primary">送出</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(".delete_button").click(function () {
                deleteMsg($(this).val());
            });

            function deleteMsg(id)
            {
                console.log('id: ', id);
                swal({
                    title: '輸入保護密碼',
                    html:
                    '<input id="pwd" class="swal2-input">',
                    confirmButtonText: '確認刪除',
                    confirmButtonColor: '#D51745',
                    showLoaderOnConfirm: true,
                    preConfirm: (login) => {
                        return new Promise(function (resolve) {
                            resolve([
                                $('#pwd').val(),
                            ])
                        })
                    },
                    allowOutsideClick: () => !swal.isLoading()
                }).then((result) => {
                    if (result.value) {
                        var data = {
                            "_token": $("input[name='_token']").val(),
                            "_method": 'DELETE',
                            "password": $('#pwd').val(),
                        };
                        $.ajax({
                            method: "DELETE",
                            url: "/message/"+id,
                            data: data,
                            success: function (resp) {
                                swal({
                                    type: 'success',
                                    title: '刪除成功',
                                    text: '3秒後跳轉...',
                                });
                                setTimeout('location.reload();', 3000)
                            },
                            error: function (resp) {
                                swal({
                                    type: 'error',
                                    title: '失敗，錯誤訊息',
                                    text: JSON.stringify(resp),
                                });
                            }
                        })
                    }
                })
            }
        </script>
    </body>
</html>
