<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Message;

class MessageBoardController extends Controller
{
    //
    public function findAll(Request $request)
    {
        $resp = Message::findAll();
        return view('message_board_full_ver', ['message_list' => $resp, 'myself_ip' => request()->ip()]);
    }

    public function create(Request $request)
    {
        Message::add($request);
        return redirect()->action('MessageBoardController@findAll');
    }

    public function destory($id, Request $request)
    {
        $msg = Message::find($id);
        if($request->input('password', '') == $msg->password)
        {
            Message::destory($id);
        }
        else
        {
//            return array(
//                "error" => "fail"
//            );
            return response("密碼錯誤", "500");
        }
//        return array(
//            "success" => "ok"
//        );
        return response("成功刪除", "200");

    }
}
