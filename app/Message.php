<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

use DB;

class Message extends Model
{
    //
    protected $table = "message";

    public static function findAll()
    {
        return Message::all();
    }

    public static function add(Request $request)
    {
        return DB::table('message')->insert([
            "name" => $request->input('name', 'robot'),
            "password" => $request->input('password', ''),
            "msg_detail" => $request->input('msg_detail', ''),
            "from_ip" => $request->ip(),
        ]);
    }

    public static function destory($id)
    {
        return DB::table('message')->where("id", "=", $id)->delete();
    }
}
